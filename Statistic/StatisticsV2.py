import datetime

statistics_data = {
    "total_count": 0,
    "total_amount": 0,
    "hour_num": 0,
    "day_num": 0,
    "month_num": 0,
    "average_count_hour": 0,
    "average_count_day": 0,
    "average_count_month": 0,
    "average_amount_hour": 0,
    "average_amount_day": 0,
    "average_amount_month": 0,
    "hours_hist": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # 24
    "week_day_hist": [0, 0, 0, 0, 0, 0, 0],  # 7
    "month_day_hist": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
# 31
}

current_data = {
    "to_hour_count": 0,
    "to_hour_amount": 0,
    "to_day_count": 0,
    "to_day_amount": 0,
    "to_month_count": 0,
    "to_month_amount": 0
}


class BulkWriter:
    def __init__(self, db, collection_name):
        self.db = db
        self.collection_name = collection_name
        self.bulk = db[collection_name].initialize_ordered_bulk_op()
        self.count = 0

    def feature_add_statistic_bulk(self, value, amount, cur_time=datetime.datetime.utcnow()):
        if value is None:
            return
        else:
            self.count += 1
            self.bulk.find({"_id": str(value)}).upsert().update(
                {"$inc": {"total_count": 1, "total_amount": int(amount),
                          "hours_hist." + str(cur_time.hour): 1,
                          "week_day_hist." + str(cur_time.weekday()): 1,
                          "month_day_hist." + str(cur_time.day): 1},
                 "$set": {"last_time": timeToString(cur_time)},
                 "$setOnInsert": {"start_time": timeToString(cur_time)}})
            if self.count % 10000 == 0:
                res = self.bulk.execute()
                if res['writeErrors'] or res['writeConcernErrors']:
                    print(res)
                self.bulk = self.db[self.collection_name].initialize_ordered_bulk_op()

    def refresh_average(self, value, average_count_60_days, average_amount, average_amount_60_days):
        self.count += 1
        self.bulk.find({"_id": str(value)}).update(
            {"$set": {"average_count_60_days": average_count_60_days,
                      "average_amount": average_amount,
                      "average_amount_60_days": average_amount_60_days}})
        if self.count % 10000 == 0:
            res = self.bulk.execute()
            if res['writeErrors'] or res['writeConcernErrors']:
                print(res)
            self.bulk = self.db[self.collection_name].initialize_ordered_bulk_op()

    def add_attributes(self, value, attrs):
        self.count += 1
        self.bulk.find({"_id": str(value)}).update(
            {"$set": attrs})
        if self.count % 10000 == 0:
            res = self.bulk.execute()
            if res['writeErrors'] or res['writeConcernErrors']:
                print(res)
            self.bulk = self.db[self.collection_name].initialize_ordered_bulk_op()

    def add_example(self, example):
        self.count += 1
        self.bulk.insert(example)
        if self.count % 10000 == 0:
            res = self.bulk.execute()
            if res['writeErrors'] or res['writeConcernErrors']:
                print(res)
            print("Done : " + str(self.count) + "          examples")
            self.bulk = self.db[self.collection_name].initialize_ordered_bulk_op()

    def execute(self):
        if self.count % 10000 != 0:
            res = self.bulk.execute()
            if res['writeErrors'] or res['writeConcernErrors']:
                print(res)


def getDateTime(timestamp):
    return datetime.datetime.utcfromtimestamp(int(timestamp) / 1000)


def getDateTimeReal(timestamp):
    return datetime.datetime.utcfromtimestamp(int(timestamp))


def timeToString(time):
    return int(time.replace(tzinfo=datetime.timezone.utc).timestamp())


def diff_hours(diff_time):
    return int((diff_time.days * 86400 + diff_time.seconds) / 3600)


def feature_add_statistic(feature_collection, value, amount, cur_time=datetime.datetime.utcnow()):
    if (value == None):
        return
    # if (feature_collection.count({"_id" : value}) == 0):
    #    data = statistics_data
    #    data.update({"_id": str(value),
    #                  "start_time": TimeToString(cur_time),
    #                  "last_time": TimeToString(cur_time)})
    #    data["total_count"] = 1
    #    data["total_amount"] = int(amount)
    #    data["hours_hist"][round(cur_time.hour/3)] = 1
    #    data["week_day_hist"][cur_time.weekday()] = 1
    #    feature_collection.insert(data)
    else:
        feature_collection.update_one({"_id": value},
                                      {"$inc": {"total_count": 1, "total_amount": int(amount),
                                                "hours_hist." + str(cur_time.hour): 1,
                                                "week_day_hist." + str(cur_time.weekday()): 1,
                                                "month_day_hist." + str(cur_time.day): 1},
                                       "$set": {"last_time": timeToString(cur_time)},
                                       "$setOnInsert": {"start_time": timeToString(cur_time)}}, True)


def refresh_average(feature_collection, value, average_count_60_days, average_amount, average_amount_60_days):
    feature_collection.update_one({"_id": value},
                                  {"$set": {"average_count_60_days": average_count_60_days,
                                            "average_amount": average_amount,
                                            "average_amount_60_days": average_amount_60_days}})


def count_average(average, current, n):
    return round((average * n + current) / (n + 1))


def add_current(feature_collection, value, amount, stat_feature_collection, stat_item,
                cur_time=datetime.datetime.utcnow()):
    if value is not None:
        cursor = feature_collection.find_one({"_id": str(value)})
        # Same Month, Day, Hour
        inc = {"to_hour_count": 1, "to_hour_amount": int(amount),
               "to_day_count": 1, "to_day_amount": int(amount),
               "to_month_count": 1, "to_month_amount": int(amount)}
        set = {"last_hour": timeToString(cur_time),
               "last_day": timeToString(cur_time),
               "last_month": timeToString(cur_time)}
        inc_stat = {}
        set_stat = {}
        nothing_changed = False
        if cursor is not None:
            # Month => Day Changed + Hour Changed
            last_hour = getDateTimeReal(cursor['last_hour'])
            last_day = getDateTimeReal(cursor['last_day'])
            last_month = getDateTimeReal(cursor['last_month'])
            if stat_item["hour_num"] == 0:
                set_stat["average_count_hour"] = cursor["to_hour_count"] + 1
                set_stat["average_amount_hour"] = cursor["to_hour_amount"] + int(amount)
            if stat_item["day_num"] == 0:
                set_stat["average_count_day"] = cursor["to_day_count"] + 1
                set_stat["average_amount_day"] = cursor["to_day_amount"] + int(amount)
            if stat_item["month_num"] == 0:
                set_stat["average_count_month"] = cursor["to_month_count"] + 1
                set_stat["average_amount_month"] = cursor["to_month_amount"] + int(amount)
            if cur_time.month - last_month.month != 0:
                del inc["to_hour_count"]
                del inc["to_hour_amount"]
                del inc["to_day_count"]
                del inc["to_day_amount"]
                del inc["to_month_count"]
                del inc["to_month_amount"]
                set["to_hour_count"] = 1
                set["to_hour_amount"] = int(amount)
                set["to_day_count"] = 1
                set["to_day_amount"] = int(amount)
                set["to_month_count"] = 1
                set["to_month_amount"] = int(amount)
                inc_stat["hour_num"] = 1
                inc_stat["day_num"] = 1
                inc_stat["month_num"] = 1
                set_stat["average_count_month"] = count_average(stat_item["average_count_month"],
                                                                cursor["to_month_count"], stat_item["month_num"])
                set_stat["average_count_day"] = count_average(stat_item["average_count_day"],
                                                              cursor["to_day_count"], stat_item["day_num"])
                set_stat["average_count_hour"] = count_average(stat_item["average_count_hour"],
                                                               cursor["to_hour_count"], stat_item["hour_num"])
                set_stat["average_amount_month"] = count_average(stat_item["average_amount_month"],
                                                                 cursor["to_month_amount"], stat_item["month_num"])
                set_stat["average_amount_day"] = count_average(stat_item["average_amount_day"],
                                                               cursor["to_day_amount"], stat_item["day_num"])
                set_stat["average_amount_hour"] = count_average(stat_item["average_amount_hour"],
                                                                cursor["to_hour_amount"], stat_item["hour_num"])
            else:
                del set["last_month"]
                if cur_time.day - last_day.day != 0:
                    # Day Changed => Hour Changed
                    del inc["to_hour_count"]
                    del inc["to_hour_amount"]
                    del inc["to_day_count"]
                    del inc["to_day_amount"]
                    set["to_hour_count"] = 1
                    set["to_hour_amount"] = int(amount)
                    set["to_day_count"] = 1
                    set["to_day_amount"] = int(amount)
                    inc_stat["hour_num"] = 1
                    inc_stat["day_num"] = 1
                    set_stat["average_count_day"] = count_average(stat_item["average_count_day"],
                                                                  cursor["to_day_count"], stat_item["day_num"])
                    set_stat["average_count_hour"] = count_average(stat_item["average_count_hour"],
                                                                   cursor["to_hour_count"], stat_item["hour_num"])
                    set_stat["average_amount_day"] = count_average(stat_item["average_amount_day"],
                                                                   cursor["to_day_amount"], stat_item["day_num"])
                    set_stat["average_amount_hour"] = count_average(stat_item["average_amount_hour"],
                                                                    cursor["to_hour_amount"], stat_item["hour_num"])
                else:
                    del set["last_day"]
                    if cur_time.hour - last_hour.hour != 0:
                        # Hour Changed
                        del inc["to_hour_count"]
                        del inc["to_hour_amount"]
                        set["to_hour_count"] = 1
                        set["to_hour_amount"] = int(amount)
                        inc_stat["hour_num"] = 1
                        set_stat["average_count_hour"] = count_average(stat_item["average_count_hour"],
                                                                       cursor["to_hour_count"], stat_item["hour_num"])
                        set_stat["average_amount_hour"] = count_average(stat_item["average_amount_hour"],
                                                                        cursor["to_hour_amount"], stat_item["hour_num"])
                    else:
                        # Time has not changed
                        del set["last_hour"]
        else:
            set_stat["hour_num"] = 0
            set_stat["day_num"] = 0
            set_stat["month_num"] = 0
            set_stat["average_count_hour"] = 1
            set_stat["average_count_day"] = 1
            set_stat["average_count_month"] = 1
            set_stat["average_amount_hour"] = int(amount)
            set_stat["average_amount_day"] = int(amount)
            set_stat["average_amount_month"] = int(amount)
        if inc_stat or set_stat:
            if inc_stat:
                stat_feature_collection.update_one({"_id": str(value)},
                                                   {"$inc": inc_stat,
                                                    "$set": set_stat}, True)
            else:
                stat_feature_collection.update_one({"_id": str(value)},
                                                   {"$set": set_stat}, True)
        if inc:
            if set:
                feature_collection.update_one({"_id": str(value)},
                                              {"$inc": inc,
                                               "$set": set}, True)
            else:
                feature_collection.update_one({"_id": str(value)},
                                              {"$inc": inc}, True)
        return {"average_count_hour": set_stat.get("average_count_hour", stat_item.get("average_count_hour", 0)),
                "average_count_day": set_stat.get("average_count_day", stat_item.get("average_count_day", 0)),
                "average_count_month": set_stat.get("average_count_month", stat_item.get("average_count_month", 0)),
                "average_amount_hour": set_stat.get("average_amount_hour", stat_item.get("average_amount_hour", 0)),
                "average_amount_day": set_stat.get("average_amount_day", stat_item.get("average_amount_day", 0)),
                "average_amount_month": set_stat.get("average_amount_month", stat_item.get("average_amount_month", 0))}
