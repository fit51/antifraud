import datetime
import json

import pymongo
from pymongo import MongoClient

from Statistic import Statistics

"""
{
"_t": 1410410315000,
"dst_account": "9833194050",
"src_account": "ZKULkCJvsEDCAXFRclFNiS76pAkWNBhz491bgRRorTH",
"_id": "tx:99999998",
"action": "ALLOW",
"src_issuer": "427229",
"ip": "94.127.90.175",
"dst_issuer": "MTS",
"amount": "10000",
"tx_status": [[1410410315000, "NEW"], [1410410316001, "PENDING"], [1410410318001, "OK"]]
}
"""

"""
Data I need
"""


def database():
    client = MongoClient('localhost', 27017)
    db = client.antifraud
    collection = db.transactions
    # for transaction in collection.find(projection={'_id': False, '_t': True}).sort('_t', pymongo.DESCENDING).limit(20):
    for transaction in collection.find().sort('_t', pymongo.ASCENDING):
        print(datetime.datetime.utcfromtimestamp(int(transaction['_t']) / 1000))


# database()

stat_items = ["ip", "src_issuer", "src_account", "dst_issuer", "dst_account"]

def getDateTime(timestamp):
    return datetime.datetime.utcfromtimestamp(int(timestamp))

def NormalizeAttr(attr):
    return round(attr*100)

def find_out_class(db, transaction):
    if transaction['tx_status'][0][1] == "FRAUD" or transaction['action'] == "DENY":
        return "FRAUD"
    else:
        if db.checklist_dst_bik_black.find_one({'_id' : transaction['dst_issuer']}) is None:
            if db.checklist_dst_black.find_one({'_id' : transaction['src_account']}) is None:
                if db.checklist_dst_black.find_one({'_id' : transaction['dst_account']}) is None:
                    return "NOT_FRAUD"
    return "FRAUD"


def emulate_live_transactions():
    client\
        = MongoClient('localhost', 27017)
    db = client.antifraud
    collection = db.transactions
    statistics_collections = {
        'ip': db["statistic_ip"],
        'src_issuer': db["statistic_src_issuer"],
        'src_account': db["statistic_src_account"],
        'dst_issuer': db["statistic_dst_issuer"],
        'dst_account': db["statistic_dst_account"]
    }
    bulk_res = Statistics.BulkWriter(db, "transactions_full_test")
    bulk = {'ip': Statistics.BulkWriter(db, "statistic_ip"),
            'src_issuer': Statistics.BulkWriter(db, "statistic_src_issuer"),
            'src_account': Statistics.BulkWriter(db, "statistic_src_account"),
            'dst_issuer': Statistics.BulkWriter(db, "statistic_dst_issuer"),
            'dst_account': Statistics.BulkWriter(db, "statistic_dst_account")}
    print(datetime.datetime.now())
    for transaction in collection.find({}, no_cursor_timeout=True).batch_size(10).sort('_t', pymongo.ASCENDING).limit(1):
        tr_time = Statistics.getDateTime(transaction['_t'])
        #add to statistic (Missing case already done)
        additional_attrs = {}
        for stat_name in stat_items:
            if transaction[stat_name] is not None:
                stat_value = str(transaction[stat_name])
                #Add statistic Online
                Statistics.feature_add_statistic(statistics_collections[stat_name], transaction[stat_name],
                                                 transaction['amount'], tr_time)
                stat_item = db["statistic_"+stat_name].find_one({'_id': stat_value})
                #count_average, Save to db just for fun
                time_stored = getDateTime(stat_item['last_time']) - getDateTime(stat_item['start_time'])
                time_stored_days = 1 if time_stored.days == 0 else time_stored.days
                average_count_60_days = round((stat_item['total_count']/time_stored_days)*60)
                average_count_60_days = 1 if average_count_60_days == 0 else average_count_60_days
                average_amount = round(stat_item['total_amount']/stat_item['total_count'])
                average_amount_60_days = round((stat_item['total_amount']/time_stored_days)*60)
                average_amount_60_days = 1 if average_amount_60_days == 0 else average_amount_60_days
                bulk[stat_name].refresh_average(stat_value, average_count_60_days, average_amount, average_amount_60_days)
                #add current
                Statistics.add_current(db["current_" + stat_name], stat_value, transaction['amount'], tr_time)
                #GetCurrentAndFormAttributes
                current_item = db["current_"+stat_name].find_one({"_id": stat_value})
                additional_attrs[stat_name+"_average_amount"] = NormalizeAttr(int(transaction['amount'])/average_amount)
                additional_attrs[stat_name+"_to_hour_amount"] = NormalizeAttr(current_item['to_hour_amount']/(average_amount_60_days/1440)) # 60*24
                additional_attrs[stat_name+"_to_day_amount"] = NormalizeAttr(current_item['to_day_amount']/(average_amount_60_days/60))
                additional_attrs[stat_name+"_to_month_amount"] = NormalizeAttr(current_item['to_month_amount']/(average_amount_60_days/2))
                additional_attrs[stat_name+"_to_hour_count"] = NormalizeAttr(current_item['to_hour_count']/(average_count_60_days/1440))
                additional_attrs[stat_name+"_to_day_count"] = NormalizeAttr(current_item['to_day_count']/(average_count_60_days/60))
                additional_attrs[stat_name+"_to_month_count"] = NormalizeAttr(current_item['to_month_count']/(average_count_60_days/2))
                additional_attrs[stat_name+"_to_hour_hist_count"] = NormalizeAttr(current_item['to_hour_count']/(
                    stat_item['hours_hist'].get(str(round(tr_time.hour / 3)), 1)/3))
                additional_attrs[stat_name+"_to_week_day_hist_count"] = NormalizeAttr(current_item['to_day_count']/(
                    stat_item['week_day_hist'].get(str(tr_time.weekday()), 1)))
                additional_attrs[stat_name+"_to_month_day_hist_count"] = NormalizeAttr(current_item['to_day_count']/(
                    stat_item['month_day_hist'].get(str(round(tr_time.day / 4)), 1)/4))

        example_class = find_out_class(db, transaction)
        del transaction['tx_status']
        transaction['class'] = example_class
        transaction.update(additional_attrs)
        bulk_res.add_example(transaction)
    bulk_res.execute()
    for key, item in bulk.items():
        item.execute()
    print(datetime.datetime.now())

def collect_statistic():
    client = MongoClient('localhost', 27017)
    db = client.antifraud
    collection = db.transactions
    bulk_ip = Statistics.BulkWriter(db, "statistic_ip")
    bulk_src_issuer = Statistics.BulkWriter(db, "statistic_src_issuer")
    bulk_src_account = Statistics.BulkWriter(db, "statistic_src_account")
    bulk_dst_issuer = Statistics.BulkWriter(db, "statistic_dst_issuer")
    bulk_dst_account = Statistics.BulkWriter(db, "statistic_dst_account")

    for transaction in collection.find().sort('_t', pymongo.ASCENDING):
        bulk_ip.feature_add_statistic_bulk(transaction['ip'], transaction['amount'],
                                           cur_time=Statistics.getDateTime(transaction['_t']))
        bulk_src_issuer.feature_add_statistic_bulk(transaction['src_issuer'], transaction['amount'],
                                                   cur_time=Statistics.getDateTime(transaction['_t']))
        bulk_src_account.feature_add_statistic_bulk(transaction['src_account'], transaction['amount'],
                                                    cur_time=Statistics.getDateTime(transaction['_t']))
        bulk_dst_issuer.feature_add_statistic_bulk(transaction['dst_issuer'], transaction['amount'],
                                                   cur_time=Statistics.getDateTime(transaction['_t']))
        bulk_dst_account.feature_add_statistic_bulk(transaction['dst_account'], transaction['amount'],
                                                    cur_time=Statistics.getDateTime(transaction['_t']))

    bulk_ip.execute()
    bulk_src_issuer.execute()
    bulk_src_account.execute()
    bulk_dst_issuer.execute()
    bulk_dst_account.execute()

    # Without bulk
    # #print(transaction)
    # Statistics.feature_add_statistic(db.statistic_ip, transaction['ip'], transaction['amount'],
    #                                  cur_time=Statistics.getDateTime(transaction['_t']))
    # Statistics.feature_add_statistic(db.statistic_src_issuer, transaction['src_issuer'], transaction['amount'],
    #                                  cur_time=Statistics.getDateTime(transaction['_t']))
    # Statistics.feature_add_statistic(db.statistic_src_account, transaction['src_account'], transaction['amount'],
    #                                  cur_time=Statistics.getDateTime(transaction['_t']))
    # Statistics.feature_add_statistic(db.statistic_dst_issuer, transaction['dst_issuer'], transaction['amount'],
    #                                  cur_time=Statistics.getDateTime(transaction['_t']))
    # Statistics.feature_add_statistic(db.statistic_dst_account, transaction['dst_account'], transaction['amount'],
    #                                  cur_time=Statistics.getDateTime(transaction['_t']))


emulate_live_transactions()


def parse():
    with open("/data/transactions_short.jsons", "r") as f:
        i = 0
        """for i,l in enumerate(f):
            pass
        print(i+1)"""
        for line in f:
            transaction = json.loads(line)
            time = datetime.datetime.utcfromtimestamp(int(transaction['_t']) / 1000)
            print(time + "\n")
            for status in transaction['tx_status']:
                print(datetime.datetime.utcfromtimestamp(int(status[0]) / 1000) + "\n")
            print("")
            if (i > 10):
                break
            i += 1
