import datetime
import json

import pymongo
from pymongo import MongoClient

from Statistic import StatisticsV2

"""
{
"_t": 1410410315000,
"dst_account": "9833194050",
"src_account": "ZKULkCJvsEDCAXFRclFNiS76pAkWNBhz491bgRRorTH",
"_id": "tx:99999998",
"action": "ALLOW",
"src_issuer": "427229",
"ip": "94.127.90.175",
"dst_issuer": "MTS",
"amount": "10000",
"tx_status": [[1410410315000, "NEW"], [1410410316001, "PENDING"], [1410410318001, "OK"]]
}
"""

"""
Data I need
"""


def database():
    client = MongoClient('localhost', 27017)
    db = client.antifraud
    collection = db.transactions
    # for transaction in collection.find(projection={'_id': False, '_t': True}).sort('_t', pymongo.DESCENDING).limit(20):
    for transaction in collection.find().sort('_t', pymongo.ASCENDING):
        print(datetime.datetime.utcfromtimestamp(int(transaction['_t']) / 1000))


# database()

stat_items = ["ip", "src_issuer", "src_account", "dst_issuer", "dst_account"]

def getDateTime(timestamp):
    return datetime.datetime.utcfromtimestamp(int(timestamp))

def normalizeAttr(attr):
    return round(attr*100)

def countHistScore(hist, index, area):
    sum = 0
    for i in range(index - area, index + area + 1):
        sum += hist.get(i%len(hist), 0.5)
    return sum/(area*2)

def find_out_class(db, transaction):
    if transaction['tx_status'][0][1] == "FRAUD" or transaction['action'] == "DENY":
        return "FRAUD"
    else:
        if db.checklist_dst_bik_black.find_one({'_id' : transaction['dst_issuer']}) is None:
            if db.checklist_dst_black.find_one({'_id' : transaction['src_account']}) is None:
                if db.checklist_dst_black.find_one({'_id' : transaction['dst_account']}) is None:
                    return "NOT_FRAUD"
    return "FRAUD"


def emulate_live_transactions():
    client \
        = MongoClient('localhost', 27017)
    db = client.antifraud
    collection = db.transactions
    statistics_collections = {
        'ip': db["statistic_ip"],
        'src_issuer': db["statistic_src_issuer"],
        'src_account': db["statistic_src_account"],
        'dst_issuer': db["statistic_dst_issuer"],
        'dst_account': db["statistic_dst_account"]
    }
    bulk_res = StatisticsV2.BulkWriter(db, "transactions_full_test")
    print(datetime.datetime.now()) # current 2 lm
    for transaction in collection.find({}, no_cursor_timeout=True).batch_size(10).sort('_t', pymongo.ASCENDING).skip(7000000).limit(1000000):
        tr_time = StatisticsV2.getDateTime(transaction['_t'])
        #add to statistic (Missing case already done)
        additional_attrs = {}
        for stat_name in stat_items:
            if transaction[stat_name] is not None:
                stat_value = str(transaction[stat_name])
                #Add statistic Online
                StatisticsV2.feature_add_statistic(statistics_collections[stat_name], stat_value,
                                                   transaction['amount'], tr_time)
                stat_item = db["statistic_"+stat_name].find_one({'_id': stat_value})
                #add current = Count average + Update Average in stat_table
                average = StatisticsV2.add_current(db["current_" + stat_name], stat_value, transaction['amount'],
                                                   statistics_collections[stat_name], stat_item, tr_time)
                average_amount = round(stat_item['total_amount']/stat_item['total_count'])
                #GetCurrentAndFormAttributes
                current_item = db["current_"+stat_name].find_one({"_id": stat_value})
                additional_attrs[stat_name+"_average_amount"] = normalizeAttr(int(transaction['amount']) / average_amount)
                additional_attrs[stat_name+"_to_hour_amount"] = normalizeAttr(current_item['to_hour_amount'] /
                                                                              average["average_amount_hour"])
                additional_attrs[stat_name+"_to_day_amount"] = normalizeAttr(current_item['to_day_amount'] /
                                                                             average["average_amount_day"])
                additional_attrs[stat_name+"_to_month_amount"] = normalizeAttr(current_item['to_month_amount'] /
                                                                               average["average_amount_month"])
                additional_attrs[stat_name+"_to_hour_count"] = normalizeAttr(current_item['to_hour_count'] /
                                                                             average["average_count_hour"])
                additional_attrs[stat_name+"_to_day_count"] = normalizeAttr(current_item['to_day_count'] /
                                                                            average["average_count_day"])
                additional_attrs[stat_name+"_to_month_count"] = normalizeAttr(current_item['to_month_count'] /
                                                                              average["average_count_month"])
                additional_attrs[stat_name+"_to_hour_hist_count"] = normalizeAttr(current_item['to_hour_count'] /
                    countHistScore(stat_item['hours_hist'], tr_time.hour, 1))
                additional_attrs[stat_name+"_to_week_day_hist_count"] = normalizeAttr(current_item['to_day_count'] /
                    countHistScore(stat_item['week_day_hist'], tr_time.weekday(), 1))
                additional_attrs[stat_name+"_to_month_day_hist_count"] = normalizeAttr(current_item['to_month_count'] /
                    countHistScore(stat_item['month_day_hist'], tr_time.day, 2))

        example_class = find_out_class(db, transaction)
        del transaction['tx_status']
        transaction['class'] = example_class
        transaction.update(additional_attrs)
        bulk_res.add_example(transaction)
    bulk_res.execute()
    print(datetime.datetime.now())

def collect_statistic():
    client = MongoClient('localhost', 27017)
    db = client.antifraud
    collection = db.transactions
    bulk_ip = StatisticsV2.BulkWriter(db, "statistic_ip")
    bulk_src_issuer = StatisticsV2.BulkWriter(db, "statistic_src_issuer")
    bulk_src_account = StatisticsV2.BulkWriter(db, "statistic_src_account")
    bulk_dst_issuer = StatisticsV2.BulkWriter(db, "statistic_dst_issuer")
    bulk_dst_account = StatisticsV2.BulkWriter(db, "statistic_dst_account")

    for transaction in collection.find().sort('_t', pymongo.ASCENDING):
        bulk_ip.feature_add_statistic_bulk(transaction['ip'], transaction['amount'],
                                           cur_time=StatisticsV2.getDateTime(transaction['_t']))
        bulk_src_issuer.feature_add_statistic_bulk(transaction['src_issuer'], transaction['amount'],
                                                   cur_time=StatisticsV2.getDateTime(transaction['_t']))
        bulk_src_account.feature_add_statistic_bulk(transaction['src_account'], transaction['amount'],
                                                    cur_time=StatisticsV2.getDateTime(transaction['_t']))
        bulk_dst_issuer.feature_add_statistic_bulk(transaction['dst_issuer'], transaction['amount'],
                                                   cur_time=StatisticsV2.getDateTime(transaction['_t']))
        bulk_dst_account.feature_add_statistic_bulk(transaction['dst_account'], transaction['amount'],
                                                    cur_time=StatisticsV2.getDateTime(transaction['_t']))

    bulk_ip.execute()
    bulk_src_issuer.execute()
    bulk_src_account.execute()
    bulk_dst_issuer.execute()
    bulk_dst_account.execute()

    # Without bulk
    # #print(transaction)
    # StatisticsV2.feature_add_statistic(db.statistic_ip, transaction['ip'], transaction['amount'],
    #                                  cur_time=StatisticsV2.getDateTime(transaction['_t']))
    # StatisticsV2.feature_add_statistic(db.statistic_src_issuer, transaction['src_issuer'], transaction['amount'],
    #                                  cur_time=StatisticsV2.getDateTime(transaction['_t']))
    # StatisticsV2.feature_add_statistic(db.statistic_src_account, transaction['src_account'], transaction['amount'],
    #                                  cur_time=StatisticsV2.getDateTime(transaction['_t']))
    # StatisticsV2.feature_add_statistic(db.statistic_dst_issuer, transaction['dst_issuer'], transaction['amount'],
    #                                  cur_time=StatisticsV2.getDateTime(transaction['_t']))
    # StatisticsV2.feature_add_statistic(db.statistic_dst_account, transaction['dst_account'], transaction['amount'],
    #                                  cur_time=StatisticsV2.getDateTime(transaction['_t']))


emulate_live_transactions()


def parse():
    with open("/data/transactions_short.jsons", "r") as f:
        i = 0
        """for i,l in enumerate(f):
            pass
        print(i+1)"""
        for line in f:
            transaction = json.loads(line)
            time = datetime.datetime.utcfromtimestamp(int(transaction['_t']) / 1000)
            print(time + "\n")
            for status in transaction['tx_status']:
                print(datetime.datetime.utcfromtimestamp(int(status[0]) / 1000) + "\n")
            print("")
            if (i > 10):
                break
            i += 1
