import datetime
import json
import pymongo
from pymongo import MongoClient
from Statistic import StatisticsV2

def getDateTime(timestamp):
    return datetime.datetime.utcfromtimestamp(int(timestamp))

def normalizeAttr(attr):
    return round(attr*100)

def countHistScore(hist, index, area):
    sum = 0
    for i in range(index - area, index + area + 1):
        sum += hist.get(i%len(hist), 0.5)
    return sum/(area*2)

from sklearn.externals import joblib
import pandas as pd

class Model:
    clf = joblib.load('D:/Users/Gamer/Documents/Antifraud/Antifraud/tree4m.pk1')
    def getX(self, dt):
        return dt.drop(["class", "_t", "dst_account", "src_account", "src_issuer", "dst_issuer", "ip"], axis=1).values
    def evaluate(self, transaction_with_atributes):
        # print(transaction_with_atributes)
        dt = pd.DataFrame(transaction_with_atributes, index=[0])
        return self.clf.predict(self.getX(dt))[0]


class Logger:
    client = MongoClient('localhost', 27017)
    db = client.antifraudprod
    stat = db["clients_stat"]

    def log(self, transaction):
        transaction.update
        self.db["history"].insert(transaction)
        if transaction['class'] == 'FRAUD':
            increment = {"recall_right": 1} if transaction['right_predicted'] else {"recall_wrong": 1}
        else:
            increment = {"conversion_right": 1} if transaction['right_predicted'] else {"conversion_wrong": 1}
        self.stat.update_one({"_id": transaction['userid']},
                             {"$inc": increment}, True)


class Processor:
    client = MongoClient('localhost', 27017)
    db = client.antifraudprod
    collection = db.transactions
    statistics_collections = {
        'ip': db["statistic_ip"],
        'src_issuer': db["statistic_src_issuer"],
        'src_account': db["statistic_src_account"],
        'dst_issuer': db["statistic_dst_issuer"],
        'dst_account': db["statistic_dst_account"]
    }
    stat_items = ["ip", "src_issuer", "src_account", "dst_issuer", "dst_account"]
    model = Model()
    log = Logger()

    def process_transaction(self, transaction, userid):
        print(datetime.datetime.now())  # current 2 lm
        tr_time = StatisticsV2.getDateTime(transaction['_t'])
        # add to statistic (Missing case already done)
        userdb = self.db["transactions_"+userid]
        additional_attrs = {}
        for stat_name in self.stat_items:
            if transaction[stat_name] is not None:
                stat_value = str(transaction[stat_name])
                #Add statistic Online
                StatisticsV2.feature_add_statistic(self.statistics_collections[stat_name], stat_value,
                                                   transaction['amount'], tr_time)
                stat_item = self.db["statistic_"+stat_name].find_one({'_id': stat_value})
                #add current = Count average + Update Average in stat_table
                average = StatisticsV2.add_current(self.db["current_" + stat_name], stat_value, transaction['amount'],
                                                   self.statistics_collections[stat_name], stat_item, tr_time)
                average_amount = round(stat_item['total_amount']/stat_item['total_count'])
                #GetCurrentAndFormAttributes
                current_item = self.db["current_"+stat_name].find_one({"_id": stat_value})
                additional_attrs[stat_name+"_average_amount"] = normalizeAttr(int(transaction['amount']) / average_amount)
                additional_attrs[stat_name+"_to_hour_amount"] = normalizeAttr(current_item['to_hour_amount'] /
                                                                              average["average_amount_hour"])
                additional_attrs[stat_name+"_to_day_amount"] = normalizeAttr(current_item['to_day_amount'] /
                                                                             average["average_amount_day"])
                additional_attrs[stat_name+"_to_month_amount"] = normalizeAttr(current_item['to_month_amount'] /
                                                                               average["average_amount_month"])
                additional_attrs[stat_name+"_to_hour_count"] = normalizeAttr(current_item['to_hour_count'] /
                                                                             average["average_count_hour"])
                additional_attrs[stat_name+"_to_day_count"] = normalizeAttr(current_item['to_day_count'] /
                                                                            average["average_count_day"])
                additional_attrs[stat_name+"_to_month_count"] = normalizeAttr(current_item['to_month_count'] /
                                                                              average["average_count_month"])
                additional_attrs[stat_name+"_to_hour_hist_count"] = normalizeAttr(current_item['to_hour_count'] /
                                                                                  countHistScore(stat_item['hours_hist'], tr_time.hour, 1))
                additional_attrs[stat_name+"_to_week_day_hist_count"] = normalizeAttr(current_item['to_day_count'] /
                                                                                      countHistScore(stat_item['week_day_hist'], tr_time.weekday(), 1))
                additional_attrs[stat_name+"_to_month_day_hist_count"] = normalizeAttr(current_item['to_month_count'] /
                                                                                       countHistScore(stat_item['month_day_hist'], tr_time.day, 2))
        # print(transaction)
        # print(additional_attrs)
        transaction.update(additional_attrs)
        answer = self.model.evaluate(transaction)
        right_predicted = answer == transaction['class']
        transaction.update({"right_predicted": right_predicted, "userid" : userid})
        self.log.log(transaction)
        return answer



