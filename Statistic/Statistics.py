import datetime

statistics_data = {
    "total_count": 0,
    "total_amount": 0,
    "average_count_60_days": 0,
    "average_amount": 0,
    "average_amount_60_days": 0,
    "hours_hist": [0, 0, 0, 0, 0, 0, 0, 0, 0],  # 24 hours divide on 3 hours
    "week_day_hist": [0, 0, 0, 0, 0, 0, 0],
    "month_day_hist": [0, 0, 0, 0, 0, 0, 0]
}

current_data = {
    "to_hour_count": 0,
    "to_hour_amount": 0,
    "to_day_count": 0,
    "to_day_amount": 0,
    "to_month_count": 0,
    "to_month_amount": 0
}


class BulkWriter:
    def __init__(self, db, collection_name):
        self.db = db
        self.collection_name = collection_name
        self.bulk = db[collection_name].initialize_ordered_bulk_op()
        self.count = 0

    def feature_add_statistic_bulk(self, value, amount, cur_time=datetime.datetime.utcnow()):
        if value is None:
            return
        else:
            self.count += 1
            self.bulk.find({"_id": str(value)}).upsert().update(
                {"$inc": {"total_count": 1, "total_amount": int(amount),
                          "hours_hist." + str(round(cur_time.hour / 3)): 1,
                          "week_day_hist." + str(cur_time.weekday()): 1,
                          "month_day_hist." + str(round(cur_time.day / 4)): 1},
                 "$set": {"last_time": timeToString(cur_time)},
                 "$setOnInsert": {"start_time": timeToString(cur_time)}})
            if self.count % 10000 == 0:
                res = self.bulk.execute()
                if res['writeErrors'] or res['writeConcernErrors']:
                    print(res)
                self.bulk = self.db[self.collection_name].initialize_ordered_bulk_op()

    def refresh_average(self, value, average_count_60_days, average_amount, average_amount_60_days):
        self.count += 1
        self.bulk.find({"_id": str(value)}).update(
            {"$set": {"average_count_60_days": average_count_60_days,
                      "average_amount": average_amount,
                      "average_amount_60_days": average_amount_60_days}})
        if self.count % 10000 == 0:
            res = self.bulk.execute()
            if res['writeErrors'] or res['writeConcernErrors']:
                print(res)
            self.bulk = self.db[self.collection_name].initialize_ordered_bulk_op()

    def add_attributes(self, value, attrs):
        self.count += 1
        self.bulk.find({"_id": str(value)}).update(
            {"$set": attrs})
        if self.count % 10000 == 0:
            res = self.bulk.execute()
            if res['writeErrors'] or res['writeConcernErrors']:
                print(res)
            self.bulk = self.db[self.collection_name].initialize_ordered_bulk_op()

    def add_example(self, example):
        self.count += 1
        self.bulk.insert(example)
        if self.count % 10000 == 0:
            res = self.bulk.execute()
            if res['writeErrors'] or res['writeConcernErrors']:
                print(res)
            print("Done : " + str(self.count) + "          examples")
            self.bulk = self.db[self.collection_name].initialize_ordered_bulk_op()

    def execute(self):
        if self.count % 10000 != 0:
            res = self.bulk.execute()
            if res['writeErrors'] or res['writeConcernErrors']:
                print(res)


def getDateTime(timestamp):
    return datetime.datetime.utcfromtimestamp(int(timestamp) / 1000)


def getDateTimeReal(timestamp):
    return datetime.datetime.utcfromtimestamp(int(timestamp))


def timeToString(time):
    return int(time.replace(tzinfo=datetime.timezone.utc).timestamp())


def diff_hours(diff_time):
    return int((diff_time.days * 86400 + diff_time.seconds) / 3600)


def feature_add_statistic(feature_collection, value, amount, cur_time=datetime.datetime.utcnow()):
    if (value == None):
        return
    # if (feature_collection.count({"_id" : value}) == 0):
    #    data = statistics_data
    #    data.update({"_id": str(value),
    #                  "start_time": TimeToString(cur_time),
    #                  "last_time": TimeToString(cur_time)})
    #    data["total_count"] = 1
    #    data["total_amount"] = int(amount)
    #    data["hours_hist"][round(cur_time.hour/3)] = 1
    #    data["week_day_hist"][cur_time.weekday()] = 1
    #    feature_collection.insert(data)
    else:
        feature_collection.update_one({"_id": str(value)},
                                      {"$inc": {"total_count": 1, "total_amount": int(amount),
                                                "hours_hist." + str(round(cur_time.hour / 3)): 1,
                                                "week_day_hist." + str(cur_time.weekday()): 1,
                                                "month_day_hist." + str(round(cur_time.day / 4)): 1},
                                       "$set": {"last_time": timeToString(cur_time)},
                                       "$setOnInsert": {"start_time": timeToString(cur_time)}}, True)


def add_current(feature_collection, value, amount, cur_time=datetime.datetime.utcnow()):
    if value is not None:
        cursor = feature_collection.find_one({"_id": str(value)})
        inc = {"to_hour_count": 1, "to_hour_amount": int(amount),
               "to_day_count": 1, "to_day_amount": int(amount),
               "to_month_count": 1, "to_month_amount": int(amount)}
        set = {"last_time": timeToString(cur_time)}
        if cursor is not None:
            last_time = getDateTimeReal(cursor['last_time'])
            if cur_time.month - last_time.month != 0:
                del inc["to_hour_count"]
                del inc["to_hour_amount"]
                del inc["to_day_count"]
                del inc["to_day_amount"]
                del inc["to_month_count"]
                del inc["to_month_amount"]
                set["to_hour_count"] = 1
                set["to_hour_amount"] = int(amount)
                set["to_day_count"] = 1
                set["to_day_amount"] = int(amount)
                set["to_month_count"] = 1
                set["to_month_amount"] = int(amount)
            else:
                if cur_time.day - last_time.day != 0:
                    del inc["to_hour_count"]
                    del inc["to_hour_amount"]
                    del inc["to_day_count"]
                    del inc["to_day_amount"]
                    set["to_hour_count"] = 1
                    set["to_hour_amount"] = int(amount)
                    set["to_day_count"] = 1
                    set["to_day_amount"] = int(amount)
                else:
                    if cur_time.hour - last_time.hour != 0:
                        del inc["to_hour_count"]
                        del inc["to_hour_amount"]
                        set["to_hour_count"] = 1
                        set["to_hour_amount"] = int(amount)
        if inc:
            feature_collection.update_one({"_id": str(value)},
                                          {"$inc": inc,
                                           "$set": set}, True)
        else:
            feature_collection.update_one({"_id": str(value)},
                                          {"$set": set}, True)
