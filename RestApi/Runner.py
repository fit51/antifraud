from flask import Flask, render_template, url_for, request, session, redirect
from flask_pymongo import PyMongo
from flask_restful import Resource, Api, reqparse
from flask_httpauth import HTTPBasicAuth
import bcrypt
from MongoSession import MongoSessionInterface
from Statistic import Evaluator



app = Flask(__name__)
api = Api(app)
auth = HTTPBasicAuth()

app.session_interface = MongoSessionInterface(db='antifraudprod')
app.config['MONGO_DBNAME'] = 'antifraudprod'
app.config['MONGO_URI'] = 'mongodb://127.0.0.1:27017/antifraudprod'

mongo = PyMongo(app)

proc = Evaluator.Processor()

parser = reqparse.RequestParser()
parser.add_argument('accessToken', type = str, help = 'No ip title provided')
parser.add_argument('_t', type = str, help = 'No ip title provided')
parser.add_argument('amount', type = str, help = 'No src_issuer title provided')
parser.add_argument('class', type = str, help = 'No src_account title provided')
parser.add_argument('ip', type = str, help = 'No src_issuer title provided')
parser.add_argument('src_issuer', type = str, help = 'No src_account title provided')
parser.add_argument('src_account', type = str, help = 'No src_account title provided')
parser.add_argument('dst_issuer', type = str, help = 'No src_account title provided')
parser.add_argument('dst_account', type = str, help = 'No src_account title provided')

parser2 = reqparse.RequestParser()
parser2.add_argument('username', type = str, help = 'No ip title provided')
parser2.add_argument('pass', type = str, help = 'No src_issuer title provided')

class AccessToken(Resource):
    def post(self):
        args = parser2.parse_args(strict=True)
        users = mongo.db.users
        login_user = users.find_one({'name' : args['username']})
        if login_user:
            if bcrypt.hashpw(args['pass'].encode('utf-8'), login_user['password']) == login_user['password']:
                session['username'] = request.form['username']
                return {"accessToken" : session.sid}
        return 'Invalid username/password combination'

stat_items = ["_t", "amount", "class", "ip", "src_issuer", "src_account", "dst_issuer", "dst_account"]
class TransactionApi(Resource):
    def post(self):
        args = parser.parse_args(strict=True)
        # if args['accessToken'] == session.sid:
        tr = {key: args[key] for key in stat_items}
        result = proc.process_transaction(tr, "test")
        return {'result': result}
        # return 'Wrong AccessToken'

api.add_resource(TransactionApi, '/validate_transaction', endpoint='transaction')
api.add_resource(AccessToken, '/sign_up', endpoint='accesstoken')

@app.route('/admin')
def admin():
    if 'username' in session:
        return 'You are logged in as ' + session['username']

    return render_template('admin.html')

@app.route('/login', methods=['POST'])
def login():
    users = mongo.db.users
    login_user = users.find_one({'name' : request.form['username']})

    if login_user:
        if bcrypt.hashpw(request.form['pass'].encode('utf-8'), login_user['password']) == login_user['password']:
            session['username'] = request.form['username']
            return redirect(url_for('admin'))

    return 'Invalid username/password combination'

@app.route('/register', methods=['POST', 'GET'])
def register():
    if request.method == 'POST':
        users = mongo.db.users
        existing_user = users.find_one({'name' : request.form['username']})

        if existing_user is None:
            hashpass = bcrypt.hashpw(request.form['pass'].encode('utf-8'), bcrypt.gensalt())
            users.insert({'name' : request.form['username'], 'password' : hashpass})
            session['username'] = request.form['username']
            return redirect(url_for('admin'))

        return 'That username already exists!'

    return render_template('register.html')

if __name__ == '__main__':
    app.secret_key = 'mysecret'
    app.run(debug=True)