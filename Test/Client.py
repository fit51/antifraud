import pymongo
from pymongo import MongoClient


# fields = [
#     "_id",
#     "ip_to_day_count",
#     "src_account_to_hour_amount",
#     "src_account_to_week_day_hist_count",
#     "dst_account_to_hour_count",
#     "dst_account_to_week_day_hist_count",
#     "ip_to_month_amount",
#     "src_account_to_hour_count",
#     "src_issuer_to_day_count",
#     "dst_account_to_month_count",
#     "src_issuer_to_month_day_hist_count",
#     "ip_to_hour_hist_count",
#     "dst_account_average_amount",
#     "src_issuer_to_month_amount",
#     "src_account_to_month_count",
#     "ip_to_day_amount",
#     "dst_issuer_to_hour_hist_count",
#     "dst_issuer_to_week_day_hist_count",
#     "src_account_to_month_amount",
#     "src_issuer_to_week_day_hist_count",
#     "dst_account_to_month_amount",
#     "dst_account_to_day_amount",
#     "src_account_to_day_amount",
#     "amount",
#     "dst_account_to_hour_hist_count",
#     "dst_account_to_month_day_hist_count",
#     "src_issuer",
#     "dst_issuer_to_month_amount",
#     "ip_to_month_count",
#     "dst_account",
#     "dst_issuer_to_hour_count",
#     "class",
#     "_t",
#     "src_account_to_hour_hist_count",
#     "dst_account_to_hour_amount",
#     "ip",
#     "ip_average_amount",
#     "ip_to_hour_amount",
#     "dst_issuer_to_day_count",
#     "src_issuer_average_amount",
#     "dst_account_to_day_count",
#     "src_issuer_to_hour_count",
#     "src_issuer_to_month_count",
#     "ip_to_hour_count",
#     "src_account_to_month_day_hist_count",
#     "ip_to_month_day_hist_count",
#     "src_issuer_to_day_amount",
#     "dst_issuer_to_day_amount",
#     "src_account_to_day_count",
#     "dst_issuer_to_hour_amount",
#     "src_account_average_amount",
#     "src_account",
#     "dst_issuer",
#     "src_issuer_to_hour_amount",
#     "ip_to_week_day_hist_count",
#     "dst_issuer_average_amount",
#     "dst_issuer_to_month_count",
#     "action",
#     "dst_issuer_to_month_day_hist_count",
#     "src_issuer_to_hour_hist_count"
# ]

import http.client, urllib.parse
import json

def emulate_live_transactions():
    client = MongoClient('localhost', 27017)
    db = client.antifraud
    collection = db.transactions_full_test
    stat_items = ["_t", "amount", "class", "ip", "src_issuer", "src_account", "dst_issuer", "dst_account"]
    connection = http.client.HTTPConnection('127.0.0.1:5000')
    params = urllib.parse.urlencode({'username': 'test', 'pass': 'password'})
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    connection.request("POST", "/sign_up", params, headers)
    response = connection.getresponse()
    if response.status == 200:
        answer = response.read().decode('utf-8')
    else:
        print("Error sign_up")
    if "accessToken" in str(answer):
        session = json.loads(answer)['accessToken']
        print(session)
        for transaction in collection.find({}).sort('_t', pymongo.ASCENDING).skip(100000).limit(10000):
            transaction_short = { key: transaction[key] for key in stat_items }
            transaction_short.update({'accessToken': session})
            tr_params = urllib.parse.urlencode(transaction_short)
            connection.request("POST", "/validate_transaction", tr_params, headers)
            response = connection.getresponse()
            if response.status == 200:
                answer = response.read().decode('utf-8')
            else:
                print("Error validate_transaction")
            if "result" in str(answer):
                verdict = json.loads(answer)['result']
                print(verdict)
            else:
                print("Error validate_transaction")
    else:
        print("Error sign_up")

emulate_live_transactions()


