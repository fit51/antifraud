Weight

>>> evaluate_classifier(dt, weight=1, max_depth=15, min_samples_split=2, max_features=None)
node_count = 1713
max_depth = 15
Accuracy = 0.952519838324
Accuracy_train = 0.985856655597
Recall_Fraud = 0.174840111115
Precision_Fraud = 0.184617944101
Recall_Not_Fraud = 0.97645689697
>>> evaluate_classifier(dt, weight=5, max_depth=15, min_samples_split=2, max_features=None)
node_count = 2093
max_depth = 15
Accuracy = 0.932639295892
Accuracy_train = 0.973496651506
Recall_Fraud = 0.269326187682
Precision_Fraud = 0.156099119769
Recall_Not_Fraud = 0.953057881157
>>> evaluate_classifier(dt, weight=10, max_depth=15, min_samples_split=2, max_features=None)
node_count = 2059
max_depth = 15
Accuracy = 0.911629177059
Accuracy_train = 0.953289920569
Recall_Fraud = 0.343372596732
Precision_Fraud = 0.124406794771
Recall_Not_Fraud = 0.929123095683
>>> evaluate_classifier(dt, weight=15, max_depth=15, min_samples_split=2, max_features=None)
node_count = 1987
max_depth = 15
Accuracy = 0.899259015839
Accuracy_train = 0.943529936568
Recall_Fraud = 0.395275920483
Precision_Fraud = 0.124209012013
Recall_Not_Fraud = 0.914774570797
>>> evaluate_classifier(dt, weight=20, max_depth=15, min_samples_split=2, max_features=None)
node_count = 1887
max_depth = 15
Accuracy = 0.896038914234
Accuracy_train = 0.935343248211
Recall_Fraud = 0.416713771978
Precision_Fraud = 0.130505215212
Recall_Not_Fraud = 0.910795705388
>>> evaluate_classifier(dt, weight=25, max_depth=15, min_samples_split=2, max_features=None)
node_count = 1845
max_depth = 15
Accuracy = 0.889458868623
Accuracy_train = 0.926333227542
Recall_Fraud = 0.451548374014
Precision_Fraud = 0.131548041542
Recall_Not_Fraud = 0.9029411396
>>> evaluate_classifier(dt, weight=30, max_depth=15, min_samples_split=2, max_features=None)
node_count = 1747
max_depth = 15
Accuracy = 0.881578800611
Accuracy_train = 0.920426547097
Recall_Fraud = 0.467619360373
Precision_Fraud = 0.126232099758
Recall_Not_Fraud = 0.894323794331
>>> evaluate_classifier(dt, weight=35, max_depth=15, min_samples_split=2, max_features=None)
node_count = 1615
max_depth = 15
Accuracy = 0.8751686882
Accuracy_train = 0.915509914963
Recall_Fraud = 0.487717626538
Precision_Fraud = 0.12235972326
Recall_Not_Fraud = 0.887097969135
>>> evaluate_classifier(dt, weight=40, max_depth=15, min_samples_split=2, max_features=None)
node_count = 1591
max_depth = 15
Accuracy = 0.870988644194
Accuracy_train = 0.908439883628
Recall_Fraud = 0.493066098173
Precision_Fraud = 0.123507178754
Recall_Not_Fraud = 0.882624373976
----------------------------
Max_depth

>>> evaluate_classifier(dt, weight=24, max_depth=None, min_samples_split=2, min_impurity_split=1e-7)
node_count = 5471
max_depth = 65
Accuracy = 0.940529468305
Accuracy_train = 0.998456659866
Precision_Fraud = 0.135754136363
Recall_Fraud = 0.176199435813
Recall_Fraud_train = 0.998660315511
Recall_Not_Fraud = 0.964056409575
Recall_Not_Fraud_train = 0.998450391957
>>> evaluate_classifier(dt, weight=24, max_depth=100, min_samples_split=2, min_impurity_split=1e-7)
node_count = 5461
max_depth = 64
Accuracy = 0.940089455504
Accuracy_train = 0.998456659866
Precision_Fraud = 0.134593601103
Recall_Fraud = 0.175870147256
Recall_Fraud_train = 0.998660315511
Recall_Not_Fraud = 0.963613162465
Recall_Not_Fraud_train = 0.998450391957
>>> evaluate_classifier(dt, weight=24, max_depth=50, min_samples_split=2, min_impurity_split=1e-7)
node_count = 5261
max_depth = 50
Accuracy = 0.939369469103
Accuracy_train = 0.996366653777
Precision_Fraud = 0.14274409056
Recall_Fraud = 0.187922826247
Recall_Fraud_train = 0.998660315511
Recall_Not_Fraud = 0.962499931702
Recall_Not_Fraud_train = 0.996296060481
>>> evaluate_classifier(dt, weight=24, max_depth=40, min_samples_split=2, min_impurity_split=1e-7)
node_count = 4741
max_depth = 40
Accuracy = 0.935569319497
Accuracy_train = 0.991033322132
Precision_Fraud = 0.139657235783
Recall_Fraud = 0.212388427705
Recall_Fraud_train = 0.998660315511
Recall_Not_Fraud = 0.957830405858
Recall_Not_Fraud_train = 0.990798573043
>>> evaluate_classifier(dt, weight=24, max_depth=30, min_samples_split=2, min_impurity_split=1e-7)
node_count = 4097
max_depth = 30
Accuracy = 0.927779134284
Accuracy_train = 0.978519982752
Precision_Fraud = 0.14467477455
Recall_Fraud = 0.270679231672
Recall_Fraud_train = 0.996762595307
Recall_Not_Fraud = 0.948006965147
Recall_Not_Fraud_train = 0.977958498615
>>> evaluate_classifier(dt, weight=24, max_depth=25, min_samples_split=2, min_impurity_split=1e-7)
node_count = 3595
max_depth = 25
Accuracy = 0.922139127075
Accuracy_train = 0.968829970617
Precision_Fraud = 0.127659263794
Recall_Fraud = 0.286424069827
Recall_Fraud_train = 0.989506785156
Recall_Not_Fraud = 0.941708896783
Recall_Not_Fraud_train = 0.968193578254
>>> evaluate_classifier(dt, weight=24, max_depth=20, min_samples_split=2, min_impurity_split=1e-7)
node_count = 2891
max_depth = 20
Accuracy = 0.912138977459
Accuracy_train = 0.954299947814
Precision_Fraud = 0.132179646035
Recall_Fraud = 0.344042838019
Recall_Fraud_train = 0.979571306945
Recall_Not_Fraud = 0.929628070961
Recall_Not_Fraud_train = 0.953522144359
>>> evaluate_classifier(dt, weight=24, max_depth=15, min_samples_split=2, min_impurity_split=1e-7)
node_count = 1871
max_depth = 15
Accuracy = 0.889328919423
Accuracy_train = 0.927879907365
Precision_Fraud = 0.129261733692
Recall_Fraud = 0.452213231837
Recall_Fraud_train = 0.969971148631
Recall_Not_Fraud = 0.902786554404
Recall_Not_Fraud_train = 0.926584429842
>>> evaluate_classifier(dt, weight=24, max_depth=10, min_samples_split=2, min_impurity_split=1e-7)
node_count = 673
max_depth = 10
Accuracy = 0.848158471757
Accuracy_train = 0.886603281358
Precision_Fraud = 0.112914735371
Recall_Fraud = 0.58249665328
Recall_Fraud_train = 0.923084235229
Recall_Not_Fraud = 0.856339403737
Recall_Not_Fraud_train = 0.885480419718
>>> evaluate_classifier(dt, weight=24, max_depth=5, min_samples_split=2, min_impurity_split=1e-7)
node_count = 53
max_depth = 5
Accuracy = 0.825708798521
Accuracy_train = 0.851633234285
Precision_Fraud = 0.115366435572
Recall_Fraud = 0.679888006001
Recall_Fraud_train = 0.79961916991
Recall_Not_Fraud = 0.830199080444
Recall_Not_Fraud_train = 0.853234189698
>>>

