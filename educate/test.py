from sklearn import preprocessing
lb = preprocessing.LabelBinarizer()
#Y = lb.fit_transform(getY(dt)).flatten()
f = np.vectorize(swap, otypes=[np.int])
Y = f(lb.fit_transform(getY(dt)).flatten())
print_pos_neg(Y)
cross_val_score(clf, getX(dt), Y, cv=4, n_jobs=4, scoring='recall')


from sklearn.metrics import confusion_matrix
from sklearn.model_selection import KFold
kf = KFold(n_splits=4)
for train, test in kf.split(getX(dt), getY(dt)):
    X_train, X_test, y_train, y_test = getX(dt)[train], getX(dt)[test], getY(dt)[train], getY(dt)[test]
    clf.fit(X_train, y_train)
    m = confusion_matrix(y_test, clf.predict(X_test), labels=['NOT_FRAUD', 'FRAUD'])
    print("Recall_Fraud = %s" % (m[1][1]/(m[1][1]+m[1][0])))
    print("Recall_NOT_Fraud = %s" % (m[0][0]/(m[0][0]+m[0][1])))