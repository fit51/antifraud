import pandas as pd
import datetime
from IPython.display import Image
from sklearn import tree
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.model_selection import train_test_split

from sklearn.model_selection import cross_val_score
import pydotplus
from sklearn import metrics

def main():
    print(datetime.datetime.now())
    #dt = pd.read_csv("/home/pavel/Documents/Antifraud/Orange/100000.tab", sep='\t', header=0, skiprows=[1,2])
    dt = pd.read_csv("/home/pavel/Documents/Antifraud/transactions_1m.csv", header=0)
    nd = dt.values
    print("Num of columns: %d" % (len(dt.columns)))
    print("Attributes:", " ,".join(dt.columns))
    print("Examples: %d" % (len(nd)))
    random_forest(dt[:10000])
print(datetime.datetime.now())

def getX(dt):
    return dt.drop(["class", "_id", "_t", "action", "dst_account", "src_account", "src_issuer", "dst_issuer", "ip"], axis=1).values


def getY(dt):
    return dt[["class"]].values.ravel()

def my_estimator():
    #dt = pd.read_csv("/home/pavel/Documents/Antifraud/transactions_1m.csv", header=0)
    dt = pd.read_csv('D:/Users/Gamer/Documents/Antifraud/Antifraud/transactions_500k.csv', header=0)
    null_arg = np.argwhere(np.isnan(getX(dt)))
    dt = dt.drop(dt.index[null_arg[0][0]])
    X_train, X_test, y_train, y_test = train_test_split(getX(dt), getY(dt), test_size=0.2)
    clf = tree.DecisionTreeClassifier()
    clf.fit(X_train, y_train)
    from sklearn.metrics import precision_score
    y_pred = clf.predict(X_test)
    pr_sc = precision_score(y_test, y_pred, pos_label='FRAUD')
    #Try cros_val
    from sklearn import preprocessing
    lb = preprocessing.LabelBinarizer()
    lb.fit(y_train)
    Y_train = lb.fit_transform(y_train).flatten()
    from sklearn.model_selection import cross_val_score

def print_pos_neg(Y):
    fraud = 0
    not_fraud = 0
    for y in Y:
        if y == 1:
            not_fraud += 1
        else:
            if y == 0:
                fraud += 1
    print("Positive: %s" % not_fraud)
    print("Negative: %s" % fraud)

def swap(x):
    return 1 - x

def my_estimator2():
    dt = pd.read_csv('D:/Users/Gamer/Documents/Antifraud/Antifraud/transactions_500k.csv', header=0)
    null_arg = np.argwhere(np.isnan(getX(dt)))
    dt = dt.drop(dt.index[null_arg[0][0]])
    from sklearn import preprocessing
    lb = preprocessing.LabelBinarizer()
    f = np.vectorize(swap, otypes=[np.int])
    Y = f(lb.fit_transform(getY(dt)).flatten())
    print_pos_neg(Y)
    clf = tree.DecisionTreeClassifier()
    cross_val_score(clf, getX(dt), Y, cv=2, n_jobs=4, scoring='precision')
    cross_val_score(clf, getX(dt), Y, cv=2, n_jobs=4, scoring='recall')




def random_forest(dt):
    disc_class = discretevalues(dt["class"])
    X = dt.drop(["class", "_id", "_t", "action", "dst_account", "src_account", "src_issuer", "dst_issuer", "ip"], axis=1).values
    Y = dt[["class"]].values.ravel()
    grouped = dt.groupby('class')
    fraud = grouped.get_group('FRAUD')
    not_fraud = grouped.get_group('NOT_FRAUD')
    X_train_fraud, X_test_fraud, y_train_fraud, y_test_fraud = train_test_split(getX(fraud), getY(fraud), test_size=0.1)
    X_train_not_fraud, X_test_not_fraud, y_train_not_fraud, y_test_not_fraud = \
        train_test_split(getX(not_fraud), getY(not_fraud), test_size=0.2)
    print(disc_class)
    clf = learn_random_forest(np.concatenate((X_train_fraud, X_train_not_fraud), axis=0), np.concatenate((y_train_fraud,\
                                                                                                          y_train_not_fraud), axis=0))
    print(clf.score(X_test_fraud, y_test_fraud))

    # scores = cross_val_score(clf, X, Y, scoring='accuracy', cv=5, n_jobs=4)
    # print(scores)
    forest = clf
    importances = forest.feature_importances_
    std = np.std([tree.feature_importances_ for tree in forest.estimators_],
                 axis=0)
    indices = np.argsort(importances)[::-1]
    # Print the feature ranking
    print("Feature ranking:")

    # for f in range(X.shape[1]):
    #     print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))

def learn_random_forest(X, Y):
    clf = RandomForestClassifier(n_estimators=20, max_features=8, max_depth=None, min_samples_split=2, random_state=0,
                                 #n_jobs=4)
                                 n_jobs=4, class_weight={'FRAUD': 24, 'NOT_FRAUD': 1})
                                 #n_jobs=4, class_weight="balanced")
    #clf = ExtraTreesClassifier(n_estimators=20, max_features=8, max_depth=None, min_samples_split=2, n_jobs=4)
    
    clf = clf.fit(X, Y)
    return clf

def learn_Tree(dt):
    disc_class = discretevalues(dt["class"])
    grouped = dt.groupby('class')
    fraud = grouped.get_group('FRAUD')
    not_fraud = grouped.get_group('NOT_FRAUD')
    X = dt.drop(["class", "_id", "_t", "action", "dst_account", "src_account", "src_issuer", "dst_issuer", "ip"], axis=1).values
    Y = dt[["class"]].values.ravel()
    print(disc_class)
    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(X, Y)
    scores = cross_val_score(clf, X, Y, scoring='accuracy', cv=5, n_jobs=4)
    print(scores)
    print_tree(clf)
    dot_data = tree.export_graphviz(clf, out_file=None,
                                    feature_names=dt.columns,
                                    class_names=list(disc_class),
                                    filled=True, rounded=True,
                                    special_characters=True)
    graph = pydotplus.graph_from_dot_data(dot_data)
    graph.write_png('/home/pavel/Documents/Antifraud/Orange/tree100.png')

def print_tree(estimator):
    n_nodes = estimator.tree_.node_count
    children_left = estimator.tree_.children_left
    children_right = estimator.tree_.children_right
    feature = estimator.tree_.feature
    threshold = estimator.tree_.threshold


    # The tree structure can be traversed to compute various properties such
    # as the depth of each node and whether or not it is a leaf.
    node_depth = np.zeros(shape=n_nodes)
    is_leaves = np.zeros(shape=n_nodes, dtype=bool)
    stack = [(0, -1)]  # seed is the root node id and its parent depth
    while len(stack) > 0:
        node_id, parent_depth = stack.pop()
        node_depth[node_id] = parent_depth + 1

        # If we have a test node
        if (children_left[node_id] != children_right[node_id]):
            stack.append((children_left[node_id], parent_depth + 1))
            stack.append((children_right[node_id], parent_depth + 1))
        else:
            is_leaves[node_id] = True

    print("The binary tree structure has %s nodes and has "
          "the following tree structure:"
          % n_nodes)
    for i in range(n_nodes):
        if is_leaves[i]:
            print("%snode=%s leaf node." % (node_depth[i] * "\t", i))
        else:
            print("%snode=%s test node: go to node %s if X[:, %s] <= %ss else to "
                  "node %s."
                  % (node_depth[i] * "\t",
                     i,
                     children_left[i],
                     feature[i],
                     threshold[i],
                     children_right[i],
                     ))
    print()

def discretevalues(examples):
    l = {}
    for ex in examples:
        if ex in l:
            l[ex] = l[ex] + 1
        else:
            l[ex] = 1
    return l


main()
