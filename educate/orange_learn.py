import Orange
import datetime

def main():
    print(datetime.datetime.now())
    data = Orange.data.Table("/home/pavel/Documents/Antifraud/Orange/1000.tab")
    print("Class:", data.domain.class_var.name)
    print("Data instances", len(data))
    n = len(data.domain.attributes)
    n_cont = sum(1 for a in data.domain.attributes if a.is_continuous)
    n_disc = sum(1 for a in data.domain.attributes if a.is_discrete)
    print("%d attributes: %d continuous, %d discrete" % (n, n_cont, n_disc))
    targets = data.domain.class_var.values
    print("Classes:", ", ".join(targets))
    for c in targets:
        num = 0
        for d in data:
            if d.get_class() == c:
                num += 1
        print(c, ": ", num)
    print(datetime.datetime.now())
    learn(data)




def learn(data):
    learner = Orange.classification.CN2UnorderedLearner()
    learner.rule_finder.search_algorithm.beam_width = 10
    # continuous value space is constrained to reduce computation time
    learner.rule_finder.search_strategy.constrain_continuous = True
    # found rules must cover at least 2 examples
    learner.rule_finder.general_validator.min_covered_examples = 2
    # found rules may combine at most 2 selectors (conditions)
    learner.rule_finder.general_validator.max_rule_length = 3
    classifier = learner(data)
    # lr = Orange.classification.LogisticRegressionLearner()
    # rf = Orange.classification.RandomForestLearner(n_estimators=20)
    res = Orange.evaluation.CrossValidation(data, learner, k=5)



main()