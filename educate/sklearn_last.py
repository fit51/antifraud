import pandas as pd
from sklearn import tree
import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
import pydotplus
from sklearn.externals import joblib
from sklearn import svm
from sklearn.metrics import f1_score


def print_pos_neg(Y):
    fraud = 0
    not_fraud = 0
    for y in Y:
        if y == 1:
            not_fraud += 1
        else:
            if y == 0:
                fraud += 1
    print("Positive: %s" % not_fraud)
    print("Negative: %s" % fraud)


def swap(x):
    return 1 - x


def getX(dt):
    return dt.drop(["class", "_id", "_t", "action", "dst_account", "src_account", "src_issuer", "dst_issuer", "ip"],
                   axis=1).values


def getY(dt):
    return dt[["class"]].values.ravel()

def discretevalues(examples):
    l = {}
    for ex in examples:
        if ex in l:
            l[ex] = l[ex] + 1
        else:
            l[ex] = 1
    return l

def print_pos_neg(Y):
    fraud = 0
    not_fraud = 0
    for y in Y:
        if y == 1:
            not_fraud += 1
        else:
            if y == 0:
                fraud += 1
    print("Positive: %s" % not_fraud)
    print("Negative: %s" % fraud)

def swap(y):
    return 1 - y

def evaluate_classifier2(dt, weight=24, max_depth=15):
    from sklearn import preprocessing
    clf = tree.DecisionTreeClassifier(max_depth=max_depth, class_weight={1: 1, 0: weight})
    lb = preprocessing.LabelBinarizer()
    Y = lb.fit_transform(getY(dt)).flatten()
    # print_pos_neg(Y)
    r_n_f = cross_val_score(clf, getX(dt), Y, cv=4, n_jobs=4, scoring='recall')
    Recall_Not_Fraud = sum(r_n_f)/len(r_n_f)
    f = np.vectorize(swap, otypes=[np.int])
    Y = f(lb.fit_transform(getY(dt)).flatten())
    clf = tree.DecisionTreeClassifier(max_depth=max_depth, class_weight={1: weight, 0: 1})
    r_f = cross_val_score(clf, getX(dt), Y, cv=4, n_jobs=4, scoring='recall')
    Recall_Fraud = sum(r_f)/len(r_f)
    print("Recall_Fraud = %s" % Recall_Fraud)
    print("Recall_Not_Fraud = %s" % Recall_Not_Fraud)

def evaluate_classifier(dt, weight=1, max_depth=None, min_samples_split=2, max_features=None,
                        min_impurity_split=1e-7):
    from sklearn.metrics import confusion_matrix
    from sklearn.model_selection import StratifiedKFold
    clf = tree.DecisionTreeClassifier(max_depth=max_depth, min_samples_split=min_samples_split,
                                      max_features=max_features, min_impurity_split=min_impurity_split,
                                      class_weight={'FRAUD': weight, 'NOT_FRAUD': 1})
    kf = StratifiedKFold(n_splits=4)  # devides all the samples in k groups (k-1 uses for learning, 1 for test)
    Recall_Fraud = 0
    Precision_Fraud = 0
    Recall_Not_Fraud = 0
    Accuracy = 0
    Accuracy_train = 0
    Recall_Fraud_train = 0
    Precision_Fraud_train = 0
    Recall_Not_Fraud_train = 0
    for train, test in kf.split(getX(dt), getY(dt)):
        X_train, X_test, y_train, y_test = getX(dt)[train], getX(dt)[test], getY(dt)[train], getY(dt)[test]
        clf.fit(X_train, y_train)
        m = confusion_matrix(y_test, clf.predict(X_test), labels=['NOT_FRAUD', 'FRAUD'])
        Recall_Fraud += m[1][1]/(m[1][1]+m[1][0])
        Precision_Fraud += m[1][1]/(m[1][1]+m[0][1])
        Recall_Not_Fraud += m[0][0]/(m[0][0]+m[0][1])
        Accuracy += (m[0][0] + m[1][1])/(m[0][0]+m[0][1] + m[1][1]+m[1][0])
        m_train = confusion_matrix(y_train, clf.predict(X_train), labels=['NOT_FRAUD', 'FRAUD'])
        Recall_Fraud_train += m_train[1][1]/(m_train[1][1]+m_train[1][0])
        Precision_Fraud_train += m_train[1][1]/(m_train[1][1]+m_train[0][1])
        Recall_Not_Fraud_train += m_train[0][0]/(m_train[0][0]+m_train[0][1])
        Accuracy_train += ( m_train[0][0] + m_train[1][1])/\
                          (m_train[0][0]+m_train[0][1] + m_train[1][1]+m_train[1][0])
    Recall_Fraud = Recall_Fraud/4
    Precision_Fraud = Precision_Fraud/4
    F_Fraud = 2* (Precision_Fraud*Recall_Fraud/(Recall_Fraud + Precision_Fraud))
    Recall_Not_Fraud = Recall_Not_Fraud/4
    Recall_Fraud_train = Recall_Fraud_train/4
    Recall_Not_Fraud_train = Recall_Not_Fraud_train/4
    Precision_Fraud_train = Precision_Fraud_train/4
    Accuracy_train = Accuracy_train/4
    Accuracy = Accuracy/4
    F_Fraud_train = 2* (Precision_Fraud_train*Recall_Fraud_train/(Recall_Fraud_train + Precision_Fraud_train))
    print("node_count = %s" % clf.tree_.node_count)
    print("max_depth = %s" % clf.tree_.max_depth)
    print("Accuracy = %s" % Accuracy)
    print("Accuracy_train = %s" % Accuracy_train)
    print("Precision_Fraud = %s" % Precision_Fraud)
    print("Recall_Fraud = %s" % Recall_Fraud)
    print("Recall_Fraud_train = %s" % Recall_Fraud_train)
    print("Recall_Not_Fraud = %s" % Recall_Not_Fraud)
    print("Recall_Not_Fraud_train = %s" % Recall_Not_Fraud_train)

dt = pd.read_csv('D:/Users/Gamer/Documents/Antifraud/Antifraud/transactions_1m.csv', header=0)
dt = pd.read_csv('D:/Users/Gamer/Documents/Antifraud/Antifraud/transactions_500k.csv', header=0)

null_arg = np.argwhere(np.isnan(getX(dt)))
dt = dt.drop(dt.index[null_arg[0][0]])
lb = preprocessing.LabelBinarizer()
f = np.vectorize(swap, otypes=[np.int])
Y = f(lb.fit_transform(getY(dt)).flatten())
print_pos_neg(Y)
clf = tree.DecisionTreeClassifier()
cross_val_score(clf, getX(dt), Y, cv=2, n_jobs=4, scoring='precision')
cross_val_score(clf, getX(dt), Y, cv=2, n_jobs=4, scoring='recall')

dt = pd.read_csv('D:/Users/Gamer/Documents/Antifraud/Antifraud/transactions_100k.csv', header=0)
null_arg = np.argwhere(np.isnan(getX(dt)))
dt = dt.drop(dt.index[null_arg[0][0]])
dt = pd.read_csv('D:/Users/Gamer/Documents/Antifraud/Antifraud/transactions_100k.csv', header=0)
disc_class = discretevalues(dt["class"])
X_train, X_test, y_train, y_test = train_test_split(getX(dt), getY(dt), test_size=0.2)
clf = tree.DecisionTreeClassifier(max_depth=5, class_weight={'FRAUD': 23, 'NOT_FRAUD': 1})
clf.fit(X_train, y_train)
precision_score(y_test, clf.predict(X_test), pos_label='FRAUD')
clf.feature_importances_
clf.tree_.node_count
clf.tree_.max_depth
dot_data = tree.export_graphviz(clf, out_file=None,
                                feature_names=dt.columns,
                                class_names=['FRAUD', 'NOT_FRAUD'],
                                filled=True, rounded=True,
                                special_characters=True)
graph = pydotplus.graph_from_dot_data(dot_data)
graph.write_png('D:/Users/Gamer/Documents/Antifraud/Antifraud/tree100k.png')

#Save classifier
joblib.dump(clf, 'D:/Users/Gamer/Documents/Antifraud/Antifraud/tree4m.pk1')

# Load classifier
clf2 = joblib.load('D:/Users/Gamer/Documents/Antifraud/Antifraud/tree4m.pk1')
